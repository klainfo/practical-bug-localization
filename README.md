practical-bug-localization
=============

AUTHOR
-----------------

[Chakkrit Tantithamthavorn](http://chakkrit.com/) <<kla@chakkrit.com>>

What's Practical Bug Localization?
-----------------

Practical bug localization is an application of these accurate techniques that requires less inspection effort in practice. For practical bug localization, there are a number of aspects which need to be investigated. As an initial step towards this goal, we investigated whether method-level bug localization is more practical than file-level bug localization. Current research has built their ground-truth datasets at only one granularity level. This did not allow us to investigate the comparative results at different granularity levels.

To get around this problem, we built a collection of 1,968 bug reports from three large open source software projects of Eclipse software. These bug reports were linked to actual relevant buggy files and relevant buggy methods based on a given bug report. 


DESIGN NOTES
-----

For {platform,pde,jdt}.tgz contains:
    
    bugs/ mines bug reports from bugzilla bug report database.
    
    code/ mines commits from git repository of a given project


An Example Bug Report
-----
     OpenDate: 2006-04-17 17:09:00 -0400
     
     FixedDate: 2006-04-20 12:02:22 -0400
     
     Severity: major
     
     BuggyMethods: 2006-01-01/ant#org.eclipse.ant.ui#Ant_Tools_Support#org#eclipse#ant#internal#ui#
                   launchConfigurations#AntLaunchDelegate.java#appendProperty
     
     Title: StringIndexOutOfBoundsException in AntLaunchDelegate appendProperty  
     
     Description: Im getting ...


Source Code Directory
-----
	[Project Name]/code/[snapshot]/[filename].java#[method_name]


Statistics Summary of Our Datasets
-----

| Project Name     | Study Period                | Number of Fixed Bug Reports| Number of  Files| Number of  Methods | SLOC      | Datasets            |
|:----------------:|:---------------------------:|:------------------:|:-----:|:--------:|:---------:|:-------------------:|
| Eclipse Platform | May 2, 2001 - Dec 31, 2012  | 744                |1,758  | 7,121    | 165,404   | platform.tgz (38MB) |
| Eclipse PDE      | June 5, 2001 - Dec 31, 2012 | 756                |4,979  | 26,339   | 271,002   | pde.tgz (39MB)      |
| Eclipse JDT      | May 24, 2001 - Dec 31, 2012 | 468                |4,222  | 49,486   | 1,076,985 | jdt.tgz (99MB)      |


License
-------
Eclipse Public License - v 1.0
http://www.eclipse.org/legal/epl-v10.html
